<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Make this iOS compatible -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title"  content="La Pizzeria Restaurant">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() ?>/apple-touch-icon.jpg">


    <!-- Make this Android compatible -->

    <meta name="theme-color" content="#a61206">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="La Pizzeria Restaurant">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() ?>/icon.png" sizes="192x192">

    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <header class="row headerbg">
  <div class="col col-sm6 col-lg-6">
     <div class="logo">
         <a href="<?php echo home_url('/') ?>">
         <img class="logoimage" src="<?php echo get_template_directory_uri() ?>/img/logo.svg"/>
       </a>
      </div>
</div>

  <div class="col col-sm6 col-lg-6" >

  </div>

</header>

<nav class="navbar navbar-expand-md navbar-light bg-light navborder" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'your-theme-slug' ); ?>">
        <span class="navbar-toggler-icon"></span>
    </button>
        <?php
        wp_nav_menu( array(
            'theme_location'    => 'header-menu',
            'depth'             => 1,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'bs-example-navbar-collapse-1',
            'menu_class'        => 'nav navbar-navm ml-auto',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker(),
        ) );
        ?>

</nav>
