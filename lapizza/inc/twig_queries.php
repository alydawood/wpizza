<?php
function get_aboutus_data() {
    $info=array();
    while (have_posts()){
        the_post();
        $aboutus=
        [
        'title' => get_the_title(),
        'content'  => get_the_content(),
        'thumbnail'  =>get_the_post_thumbnail_url(get_the_ID()),
        'image1'  =>get_field('image1'),
        'description1'  =>get_field('description_1'),
        'image2'  =>get_field('image2'),
        'description2'  =>get_field('description_2'),
        'image3'  =>get_field('image3'),
        'description3'  =>get_field('description_3'),


      ] ;
      array_push($info,$aboutus);
     }
     wp_reset_postdata();
     return   $info;
  }

  function get_specialties_data($category_name,$num=10) {
    $items=array();
    $args = array(
        'post_type' => 'specialties',
        'posts_per_page' => $num,
        'orderby' => 'title',
        'order' => 'ASC',
        'category_name' => $category_name

    );
    $specialties = new WP_Query($args);

    while($specialties->have_posts() ){
        $specialties->the_post();
        $item=
        [
            'thumbnail'  =>get_the_post_thumbnail_url(get_the_ID()),
            'title' => get_the_title(),
            'price'  =>get_field('price'),
            'content'  => get_the_content(),
            'permalink'  => get_the_content()


        ];
        array_push($items,$item);
    }
    wp_reset_postdata();
    return   $items;

  }
  function get_blog_data() {

    $blogs=array();
    while (have_posts()){
        the_post();
        $blog=
        [
        'permalink'=>get_the_permalink(),
        'title'=>get_the_title(),
        'thumbnail'  =>get_the_post_thumbnail_url(get_the_ID()),
        'time'  =>get_the_time('M'),
        'author'  =>get_the_author(),
        'excerpt'=>get_the_excerpt()

      ] ;
      array_push($blogs,$blog);
     }
     wp_reset_postdata();

     return $blogs;

  }

  function get_single_blog_data() {
    $blog=array();
    while(have_posts())
        {
             the_post();
             $theblog=
            [
             'title'=> get_the_title(),
             'time'=> get_the_time('M'),
              'author'=> get_the_author(),
             'content'=> get_the_content()

            ];
            array_push($blog,$theblog);

        }
        wp_reset_postdata();
    return   $blog;
  }

  function get_reservation_thumbnail() {
    $thumbnail = "";
    while(have_posts())
    {
        the_post();
        $thumbnail = get_the_post_thumbnail_url(get_the_ID());

    }
    wp_reset_postdata();

    return   $thumbnail;
  }
?>
