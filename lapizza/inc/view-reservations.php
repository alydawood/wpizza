<?php
function lapizzeria_options() {
    add_menu_page('La Pizzeria', 'Manage Reservations', 'administrator', 'lapizzeria_options', 'lapizzeria_reservations', '', 20 );

   // add_submenu_page('lapizzeria_options', 'Reservations', 'Reservations', 'administrator', 'lapizzeria_reservations','lapizzeria_reservations' );
}
add_action('admin_menu', 'lapizzeria_options');

function lapizzeria_settings() {

  /* Google Maps Group
  register_setting('lapizzeria_options_gmaps', 'lapizzeria_gmap_latitude');
  register_setting('lapizzeria_options_gmaps', 'lapizzeria_gmap_longitude');
  register_setting('lapizzeria_options_gmaps', 'lapizzeria_gmap_zoom');
  register_setting('lapizzeria_options_gmaps', 'lapizzeria_gmap_apikey');

  // Information Group
  register_setting('lapizzeria_options_info', 'lapizzeria_location');
  register_setting('lapizzeria_options_info', 'lapizzeria_phonenumber');
*/
}

//add_action('init', 'lapizzeria_settings');

function lapizzeria_get_reservations() {
    global $wpdb;
    $table = $wpdb->prefix . 'reservations';
    $reservations = $wpdb->get_results("SELECT * FROM $table", ARRAY_A);
    $items=array();
    foreach($reservations as $reservation){
        $item=[
        'id' => $reservation['id'],
       'name' =>$reservation['name'],
       'date' =>$reservation['date'],
       'email' =>$reservation['email'],
       'phone' =>$reservation['phone'],
       'message' =>$reservation['message']
       ];
       array_push($items,$item);

    }
    return $items;
}


function lapizzeria_reservations() {
    $reservations['reservations']=lapizzeria_get_reservations();
    Timber::render('show-reservations.twig', $reservations);
  }


 ?>
