<?php
/*
* Template Name: Our Menu
*/

get_header(); ?>

<?php
  $pizzas['items']=get_specialties_data('pizza') ;
 Timber::render('specialities.twig', $pizzas);


  $others['items']=get_specialties_data('others') ;
  Timber::render('specialities.twig', $others);

?>
<?php get_footer(); ?>
