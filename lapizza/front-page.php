<?php get_header(); ?>
<?php
while(have_posts())
{
the_post();
$backgorundurl=get_the_post_thumbnail_url(get_the_ID());
$content=get_the_content();
}
wp_reset_postdata();
?>
<style>
body {
    background: url(<?php echo $backgorundurl ?>) no-repeat center center fixed;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
</style>
<div class="container">

<div class="row bgwhite" style="position:relative; top:-8px">
<div class="col-12  mt-5">
<h2 class="text-center front-title mt-1"><?php echo esc_html( get_option('blogdescription') ); ?></h2>
</div>
<div class="col-12">
<?php  echo $content; ?>
<br/>
<?php $url = get_page_by_title('About Us'); ?>
<div class="text-center mt-1 mb-1">
<a class="btn btn-readmore" href="<?php echo get_permalink($url->ID);  ?>">more info</a>
</div>
</div>
</div>


<?php
$pizzas['items']=get_specialties_data('pizza',$num=3);
Timber::render('front-specialities.twig', $pizzas);

?>


<div class="row bgwhite">
<div class="col-12">
          <h2 class="front-title mt-1">Gallery</h2>
          <?php
            $url = get_page_by_title('Gallery');
            echo get_post_gallery($url->ID);
           ?>
</div>
</div>



</div>

<?php get_footer(); ?>
