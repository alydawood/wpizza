<?php get_header(); ?>

    <?php while(have_posts()): the_post(); ?>


      <div class="container">
          <div class="row">
          <div class="col-sm-12">
          <main class="text-center content-text clear">
          <h2 class="mt-5 mb-5 page-title"><?php the_title(); ?></h2>
              <?php the_content(); ?>
          </main>
          </div>
          </div>
      </div>
    <?php endwhile; ?>

<?php get_footer(); ?>
