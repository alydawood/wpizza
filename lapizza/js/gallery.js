$ = jQuery.noConflict();
$(document).ready(function() {

   // Fluid box plugin
    jQuery('.gallery a').each(function() {
       jQuery(this).attr({'data-fluidbox': ''});
    });

    if(jQuery('[data-fluidbox]').length > 0 ) {
        jQuery('[data-fluidbox]').fluidbox();
    }
});
