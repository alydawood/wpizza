<?php get_header(); ?>
<?php
        $blog_page = get_option('page_for_posts');
        $image = get_post_thumbnail_id($blog_page);
        $image = wp_get_attachment_image_src($image, 'full');
     ?>
<style>
body {
    background: url(<?php echo $image[0] ?>) no-repeat center center fixed;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
</style>

<?php
$blogs['blogs']=get_blog_data();
Timber::render('blogs.twig', $blogs);

?>

     <?php get_footer(); ?>
