<?php get_header(); ?>
<div class="container">
<div class="row ">
<div class="col col-sm-12  col-md-12 col-lg-12">
<?php
  $theblog['info']=get_single_blog_data();
   Timber::render('single-blog.twig', $theblog);
?>
<div class="container comments">
<?php comment_form(); ?>
<div class="container comment-list">
          <ol class="commentlist">
              <?php
                  $comments = get_comments(array(
                      'post_id' => $post->ID,
                      'status'  => 'approve'
                  ));
                  wp_list_comments(array(
                      'per_page' => 10,
                      'reverse_top_level' => false
                  ), $comments );

              ?>

          <ol>
      </div>
 </div>
</div>
</div>
</div>
<?php get_footer(); ?>
